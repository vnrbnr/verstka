function randomInteger(max) {
  var rand = Math.random() * (max + 1);
  rand = Math.round(rand);
  return rand;
}

function party() {
  document.querySelector('body').style.background = `rgb(
    ${randomInteger(255)},
    ${randomInteger(255)},
    ${randomInteger(255)}
    )`
}

function emoji() {
  var x;
  var y;
  x = randomInteger(19)*130;
  y = randomInteger(19)*130;
  document.querySelector('.emoji').style.backgroundPosition = `-${x}px -${y}px`
}

document.querySelector('body').addEventListener('click', emoji)
document.querySelector('body').addEventListener('mousemove', party)
