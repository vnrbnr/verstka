window.onscroll = function() {
    scrollFunction();
};

var arrow = document.querySelector(".arrow-top");
function scrollFunction() {
    if (document.documentElement.scrollTop > 50) {
        arrow.classList.add("visible");
    } else {
        arrow.classList.remove("visible");
    }
}

var buttonUp = document.querySelector(".arrow-top");
buttonUp.addEventListener("click", function () {
    document.documentElement.scrollTop = 0;    
});